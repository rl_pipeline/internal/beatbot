# [beatbot](https://gitlab.com/rl_pipeline/internal/beatbot.git)

### Overview
Beatbot is a simple web application with a client-server model to check a heartbeat on 
the network, where the client sends UDP packets and the server listens for such 
UDP packets. While the server is listening, it also actively checks whether the 
clients are still active or not.

##### Beatbot Server
* The beabot server is run on top Flask web framework.
* Supports RESTful API that uses REST architecture to handle request (CRUD operations) 
  on the frontend.
* The API has two endpoints: /beatbot/hosts and /beatbot/hosts/<string: host>
* Uses Redis for the persistent database.

##### Beatbot Client
* Uses the builtin python socket module for sending UDP packets to the server.
* Supports client API to perform requests to the server.

### Disclaimer
This was built and tested under:
* cmake-3.14.7
* cmake_modules-1.0.1
* docker-19.03.12
* docker-compose 1.26.2
* GNU Make 4.1
* python-2.7.18
* Ubuntu 18.04.3 LTS

### Prerequisites
* GNU Make 4
* cmake-3.14
* docker-19
* docker-compose-1.26

### How to Build Docker Image
```bash
git clone https://gitlab.com/rl_pipeline/internal/beatbot.git
cd beatbot
git checkout tags/<tagname>
```

###### Build the server docker image
```bash
./bootstraps/build_server.sh
```

###### Build the client docker image
```bash
./bootstraps/build_client.sh
```

### How to Run Server Container
###### Create docker network
```bash
docker network create beatbotnet
```

###### Run server container
```bash
./bootstraps/run_server.sh
```

### How to Run Client Container
###### Create docker network
```bash
docker network create beatbotnet
```

###### Run client container
```bash
./bootstraps/run_client.sh <your_server_ip_address> <beatbot_server_port>
```

Default beatbot server port is 8989. For example:
```bash
./bootstraps/run_client.sh 192.168.0.21 8989
```