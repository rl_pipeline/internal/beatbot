import httplib

import mock
import pytest

import beatbot.constants as constant
import beatbot.exceptions as exception
import beatbot.server.api as server_api


def test_hosts_get(hosts, resources):
    assert hosts.get() == (resources, httplib.OK)


def test_hosts_get_connection_error(hosts, mocked_logger, mocked_flask):
    hosts.manager.get_resources.side_effect = (
        exception.ServerCacheConnectionError
    )
    hosts.get()
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_hosts_get_response_error(
    hosts, mocked_flask, mocked_logger
):
    hosts.manager.get_resources.side_effect = exception.ServerCacheResponseError
    hosts.get()
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_hosts_post_response_error(hosts, mocked_flask, mocked_logger):
    with mock.patch("beatbot.server.api.parse_post_args") as mock_parse_post_args:
        hosts.manager.set_resource.side_effect = exception.ServerCacheResponseError
        hosts.post()
        mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_hosts_post_connection_error(hosts, mocked_flask, mocked_logger):
    with mock.patch("beatbot.server.api.parse_post_args") as mock_parse_post_args:
        hosts.manager.set_resource.side_effect = (
            exception.ServerCacheConnectionError
        )
        hosts.post()
        mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_hosts_delete_respond_error(hosts, mocked_flask, mocked_logger):
    hosts.manager.delete_resources.side_effect = (
        exception.ServerCacheResponseError
    )
    hosts.delete()
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_hosts_delete_connection_error(hosts, mocked_flask, mocked_logger):
    hosts.manager.delete_resources.side_effect = (
        exception.ServerCacheConnectionError
    )
    hosts.delete()
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_host_get(host, primary_key, resource):
    assert host.get(primary_key) == (resource, httplib.OK)


def test_host_get_not_found(
    host, mocked_flask, mocked_logger, primary_key,
):
    host.manager.get_resource.return_value = None
    host.get(primary_key)
    mocked_flask.abort.assert_called()


def test_host_put_connection_error(
    host, mocked_flask, mocked_logger, primary_key,
):
    with mock.patch("beatbot.server.api.parse_put_args") as mock_parse_put_args:
        host.manager.set_resource.side_effect = exception.ServerCacheConnectionError
        host.put(primary_key)
        mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_host_put_response_error(
    host, mocked_flask, mocked_logger, primary_key,
):
    with mock.patch("beatbot.server.api.parse_put_args") as mock_parse_put_args:
        host.manager.set_resource.side_effect = exception.ServerCacheResponseError
        host.put(primary_key)
        mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_host_delete_connection_error(
    host, mocked_flask, primary_key, mocked_logger
):
    host.manager.delete_resource.side_effect = (
        exception.ServerCacheConnectionError
    )
    host.delete(primary_key)
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)


def test_host_delete_response_error(
    host,
    mocked_flask,
    mocked_logger,
    primary_key,
):
    host.manager.delete_resource.side_effect = exception.ServerCacheResponseError
    host.delete(primary_key)
    mocked_flask.abort.assert_called_with(httplib.INTERNAL_SERVER_ERROR)
