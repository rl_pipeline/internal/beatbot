import pytest
import redis

import beatbot.server.cache as server_cache
import beatbot.exceptions as exception


def test_beatbot_cache_get_one(beatbot_cache, primary_key, resource, cache_resource):
    beatbot_cache._cache.get.return_value = cache_resource
    assert beatbot_cache.get_one(primary_key) == resource


def test_beatbot_cache_get_one_response_error(beatbot_cache, primary_key):
    beatbot_cache._cache.get.side_effect = redis.exceptions.ResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        beatbot_cache.get_one(primary_key)


def test_beatbot_cache_get_one_connection_error(beatbot_cache, primary_key):
    beatbot_cache._cache.get.side_effect = redis.exceptions.ConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        beatbot_cache.get_one(primary_key)


def test_beatbot_cache_set(beatbot_cache, primary_key, cache_resource, resource):
    beatbot_cache._cache.get.return_value = cache_resource
    beatbot_cache.set(primary_key, resource)
    assert beatbot_cache.get_one(primary_key) == resource


def test_beatbot_cache_set_response_error(beatbot_cache, primary_key, resource):
    beatbot_cache._cache.set.side_effect = redis.exceptions.ResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        beatbot_cache.set(primary_key, resource)


def test_beatbot_cache_set_connection_error(beatbot_cache, primary_key, resource):
    beatbot_cache._cache.set.side_effect = redis.exceptions.ConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        beatbot_cache.set(primary_key, resource)


def test_beatbot_cache_get(beatbot_cache, primary_key, cache_resource, resource, resources):
    beatbot_cache._cache.keys.return_value = [primary_key]
    beatbot_cache._cache.get.return_value = cache_resource
    assert beatbot_cache.get() == resources


def test_beatbot_cache_get_response_error(beatbot_cache):
    beatbot_cache._cache.keys.side_effect = redis.exceptions.ResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        beatbot_cache.get()


def test_beatbot_cache_get_connection_error(beatbot_cache):
    beatbot_cache._cache.keys.side_effect = redis.exceptions.ConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        beatbot_cache.get()


def test_beatbot_cache_delete_response_error(beatbot_cache, primary_key):
    beatbot_cache._cache.keys.return_value = [primary_key]
    beatbot_cache._cache.delete.side_effect = redis.exceptions.ResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        beatbot_cache.delete()


def test_beatbot_cache_delete_connection_error(beatbot_cache, primary_key):
    beatbot_cache._cache.keys.return_value = [primary_key]
    beatbot_cache._cache.delete.side_effect = redis.exceptions.ConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        beatbot_cache.delete()


def test_beatbot_cache_delete_one_response_error(beatbot_cache, primary_key):
    beatbot_cache._cache.delete.side_effect = redis.exceptions.ResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        beatbot_cache.delete_one(primary_key)


def test_beatbot_cache_delete_one_connection_error(beatbot_cache, primary_key):
    beatbot_cache._cache.delete.side_effect = redis.exceptions.ConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        beatbot_cache.delete_one(primary_key)
