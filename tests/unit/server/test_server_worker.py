import threading

import mock

import beatbot.constants as constant
import beatbot.server.worker


def test_listener_threading_event_property(listener_threading):
    assert isinstance(listener_threading.event, threading._Event)


def test_listener_threading_manager_property(listener_threading):
    manager_mock = mock.Mock(name="manager")
    listener_threading._manager = manager_mock
    assert listener_threading.manager == manager_mock


def test_listener_threading_updater_thread_property(listener_threading):
    checker_thread_mock = mock.Mock(name="checker_thread")
    listener_threading._checker_thread = checker_thread_mock
    assert listener_threading.checker_thread == checker_thread_mock


def test_checker_threading_event_property(checker_threading):
    event_mock = mock.Mock(name="threading_event")
    checker_threading._event = event_mock
    assert checker_threading.event == event_mock


def test_get_listener(resource_manager):
    listener = beatbot.server.worker.get_listener(
        constant.LISTENER_HOST, constant.LISTENER_PORT, resource_manager
    )
    assert isinstance(listener, beatbot.server.worker.ListenerThreading)
