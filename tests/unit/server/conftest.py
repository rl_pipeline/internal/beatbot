import cPickle

import mock
import pytest
import redis

import beatbot.constants as constant
import beatbot.server.api
import beatbot.server.cache as server_cache
import beatbot.server.utils
import beatbot.server.worker


@pytest.fixture
def mocked_hosts_manager(resources):
    mock_manager = mock.Mock(name="mock_hosts_manager")
    mock_manager.get_resources.return_value = resources
    return mock_manager


@pytest.fixture
def mocked_host_manager(resource):
    mock_manager = mock.Mock(name="mock_host_manager")
    mock_manager.get_resource.return_value = resource
    return mock_manager


@pytest.fixture
def hosts(mocked_hosts_manager):
    beatbot.server.api.Hosts.manager = mocked_hosts_manager
    return beatbot.server.api.Hosts()


@pytest.fixture
def host(mocked_host_manager):
    beatbot.server.api.Host.manager = mocked_host_manager
    return beatbot.server.api.Host()


@pytest.fixture
def primary_key():
    return "127.0.1.1"


@pytest.fixture
def resource():
    return {"last_connected": 1607032848.721141, "state": True}


@pytest.fixture
def resources(primary_key):
    return {primary_key: {"last_connected": 1607032848.721141, "state": True}}


@pytest.fixture
def mocked_strict_redis():
    with mock.patch("beatbot.server.cache.redis") as mock_redis:
        mock_strict_redis = mock.Mock(name="strict_redis")
        mock_redis.exceptions.ResponseError = redis.exceptions.ResponseError
        mock_redis.exceptions.ConnectionError = redis.exceptions.ConnectionError
        mock_redis.StrictRedis.return_value = mock_strict_redis
        yield mock_redis


@pytest.fixture
def beatbot_cache(primary_key, mocked_strict_redis):
    return server_cache.BeatbotCache(primary_key, 6379, "beatbot-pass")


@pytest.fixture
def mocked_beatbot_cache(resource, resources):
    mock_cache = mock.Mock(name="beatbot_cache")
    mock_cache.get_one.return_value = resource
    mock_cache.get.return_value = resources
    return mock_cache


@pytest.fixture
def cache_resource(resource):
    return cPickle.dumps(resource)


@pytest.fixture
def resource_manager(mocked_beatbot_cache):
    return beatbot.server.utils.ResourceManager(mocked_beatbot_cache)


@pytest.fixture
def listener_threading(resource_manager):
    thread = beatbot.server.worker.ListenerThreading(
        resource_manager, constant.LISTENER_HOST, constant.LISTENER_PORT
    )
    thread.event.set()
    return thread


@pytest.fixture
def checker_threading(resource_manager):
    thread = beatbot.server.worker.CheckerThreading(resource_manager, interval=30)
    thread.event.set()
    return thread


@pytest.fixture
def mocked_flask():
    with mock.patch("beatbot.server.api.flask") as mock_flask:
        yield mock_flask


@pytest.fixture
def mocked_logger():
    with mock.patch("beatbot.server.api.logger") as mock_logger:
        yield mock_logger
