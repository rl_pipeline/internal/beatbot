import pytest

import beatbot.exceptions as exception


def test_resource_manager_get_resource(
    mocked_beatbot_cache, resource_manager, primary_key, resource
):
    assert resource_manager.get_resource(primary_key) == resource


def test_resource_manager_get_resource_connection_error(
    mocked_beatbot_cache, resource_manager, primary_key
):
    mocked_beatbot_cache.get_one.side_effect = exception.ServerCacheConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        resource_manager.get_resource(primary_key)


def test_resource_manager_get_resource_response_error(
    mocked_beatbot_cache, resource_manager, primary_key
):
    mocked_beatbot_cache.get_one.side_effect = exception.ServerCacheResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        resource_manager.get_resource(primary_key)


def test_resource_manager_get_resources(resource_manager, resources):
    assert resource_manager.get_resources() == resources


def test_resource_manager_get_resources_connection_error(
    mocked_beatbot_cache, resource_manager
):
    mocked_beatbot_cache.get.side_effect = exception.ServerCacheConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        resource_manager.get_resources()


def test_resource_manager_get_resources_response_error(
    mocked_beatbot_cache, resource_manager
):
    mocked_beatbot_cache.get.side_effect = exception.ServerCacheResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        resource_manager.get_resources()


def test_resource_manager_set_resource_connection_error(
    mocked_beatbot_cache, resource_manager, primary_key, resource
):
    mocked_beatbot_cache.set.side_effect = exception.ServerCacheResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        resource_manager.set_resource(primary_key, resource)


def test_resource_manager_delete_resources_connection_error(
    mocked_beatbot_cache, resource_manager
):
    mocked_beatbot_cache.delete.side_effect = exception.ServerCacheConnectionError
    with pytest.raises(exception.ServerCacheConnectionError):
        resource_manager.delete_resources()


def test_resource_manager_delete_resources_response_error(
    mocked_beatbot_cache, resource_manager
):
    mocked_beatbot_cache.delete.side_effect = exception.ServerCacheResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        resource_manager.delete_resources()


def test_resource_manager_delete_resource_connection_error(
    mocked_beatbot_cache, resource_manager, primary_key
):
    mocked_beatbot_cache.delete_one.side_effect = exception.ServerCacheResponseError
    with pytest.raises(exception.ServerCacheResponseError):
        resource_manager.delete_resource(primary_key)
