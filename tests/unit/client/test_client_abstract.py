import beatbot.constants as constant


def test_abstract_entity_domain_property(abstract_entity, primary_key):
    assert abstract_entity.domain == "{schema}{host}:{port}".format(
        schema=constant.URL_SCHEME,
        host=primary_key,
        port=constant.DEFAULT_FLASK_PORT
    )


def test_abstract_entity_hosts_path_property(abstract_entity):
    assert abstract_entity.hosts_path == "{}{}".format(
        abstract_entity.domain, constant.HOSTS_PATH
    )
