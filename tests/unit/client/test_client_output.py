import httplib
import os

import pytest

import beatbot.constants as constant


def test_output_entity_get_heartbeats_ok(output_entity, mocked_requests, resources):
    assert output_entity.get_heartbeats() == resources


def test_output_entity_get_heartbeats_not_found(
    output_entity, mocked_requests, respond_get
):
    respond_get.status_code = httplib.NOT_FOUND
    assert output_entity.get_heartbeats() == {}
    mocked_requests.get.assert_called_with(output_entity.hosts_path)


def test_output_entity_get_heartbeats_catch_exception(
    output_entity, mocked_logger, mocked_requests
):
    mocked_requests.get.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.get_heartbeats()


def test_output_entity_get_heartbeat_ok(
    output_entity, primary_key, mocked_requests, resource, respond_get_one
):
    mocked_requests.get.return_value = respond_get_one
    assert output_entity.get_heartbeat(primary_key) == resource


def test_output_entity_get_heartbeat_not_found(
    output_entity, primary_key, mocked_requests, respond_get_one
):
    respond_get_one.status_code = httplib.NOT_FOUND
    mocked_requests.get.return_value = respond_get_one
    assert output_entity.get_heartbeat(primary_key) == {}


def test_output_entity_get_heartbeat_catch_exception(
    output_entity, primary_key, mocked_logger, mocked_requests
):
    mocked_requests.get.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.get_heartbeat(primary_key)


def test_output_entity_insert_heartbeat(output_entity, fields, mocked_requests):
    assert output_entity.insert_heartbeat(fields) == fields


def test_output_entity_insert_heartbeat_catch_exception(
    output_entity, fields, mocked_logger, mocked_requests
):
    mocked_requests.post.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.insert_heartbeat(fields)


def test_output_entity_update_heartbeat(
    output_entity, primary_key, fields, mocked_requests, respond_put
):
    fields.pop(constant.PRIMARY_KEY)
    assert output_entity.update_heartbeat(primary_key, fields) == respond_put.json()


def test_output_entity_update_heartbeat_not_found(
    output_entity, primary_key, fields, mocked_requests, respond_put
):
    respond_put.status_code = httplib.NOT_FOUND
    fields.pop(constant.PRIMARY_KEY)
    assert output_entity.update_heartbeat(primary_key, fields) == {}


def test_output_entity_update_heartbeat_catch_exception(
    output_entity, mocked_logger, mocked_requests, fields, primary_key
):
    fields.pop(constant.PRIMARY_KEY)
    mocked_requests.put.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.update_heartbeat(primary_key, fields)


def test_output_entity_delete_heartbeats(output_entity, mocked_requests):
    assert output_entity.delete_heartbeats() == httplib.OK


def test_output_entity_delete_heartbeats_catch_exception(
    output_entity, mocked_logger, mocked_requests
):
    mocked_requests.delete.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.delete_heartbeats()


def test_output_entity_delete_heartbeat(
    output_entity, mocked_requests, primary_key, respond_delete
):
    assert output_entity.delete_heartbeat(primary_key) == respond_delete.json()


def test_output_entity_delete_heartbeat_not_found(
    output_entity, mocked_requests, primary_key, respond_delete
):
    respond_delete.status_code = httplib.NOT_FOUND
    assert output_entity.delete_heartbeat(primary_key) == {}


def test_output_entity_delete_heartbeat_catch_exception(
    output_entity, mocked_logger, mocked_requests, primary_key
):
    mocked_requests.delete.side_effect = mocked_requests.exceptions.ConnectionError
    with pytest.raises(mocked_requests.exceptions.ConnectionError):
        output_entity.delete_heartbeat(primary_key)
