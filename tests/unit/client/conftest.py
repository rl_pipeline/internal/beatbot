import httplib

import mock
import pytest
import requests

import beatbot.constants as constant
import beatbot.client.entity.abstract
import beatbot.client.entity.output


@pytest.fixture
def primary_key():
    return "127.0.0.1"


@pytest.fixture
def resource():
    return {"last_connected": 1607032848.721141, "state": True}


@pytest.fixture
def resources(primary_key):
    return {primary_key: {"last_connected": 1607032848.721141, "state": True}}


@pytest.fixture
def respond_get(resources):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return resources

    return Respond()


@pytest.fixture
def respond_get_one(resource):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return resource

    return Respond()


@pytest.fixture
def respond_post(fields):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return fields

    return Respond()


@pytest.fixture
def respond_put(primary_key, resource):
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            resource["url"] = primary_key
            return resource

    return Respond()


@pytest.fixture
def respond_delete():
    class Respond(object):
        def __init__(self):
            self.status_code = httplib.OK

        def json(self):
            return httplib.OK

    return Respond()


@pytest.fixture
def abstract_entity(primary_key):
    return beatbot.client.entity.abstract.AbstractEntity(primary_key)


@pytest.fixture
def output_entity(primary_key):
    return beatbot.client.entity.output.OutputEntity(primary_key)


@pytest.fixture
def fields(primary_key, resource):
    fields = resource.copy()
    fields[constant.PRIMARY_KEY] = primary_key
    return fields


@pytest.fixture
def mocked_requests(resource, respond_delete, respond_get, respond_post, respond_put):
    with mock.patch("beatbot.client.entity.output.requests") as mock_requests:
        mock_requests.get = mock.Mock(return_value=respond_get)
        mock_requests.post = mock.Mock(return_value=respond_post)
        mock_requests.put = mock.Mock(return_value=respond_put)
        mock_requests.delete = mock.Mock(return_value=respond_delete)
        mock_requests.exceptions.ConnectionError = requests.exceptions.ConnectionError
        yield mock_requests


@pytest.fixture
def host_server():
    return "192.168.0.21"


@pytest.fixture
def mocked_worker_socket():
    with mock.patch("beatbot.client.worker.socket") as mock_socket:
        mock_socket.socket.return_value = mock.Mock(name="socket")
        yield mock_socket


@pytest.fixture
def sender_threading(host_server, mocked_worker_socket):
    thread = beatbot.client.worker.SenderThreading(
        host_server, constant.LISTENER_PORT
    )
    thread.event.set()
    return thread


@pytest.fixture
def mocked_logger():
    with mock.patch("beatbot.client.entity.output.logger") as mock_logger:
        yield mock_logger
