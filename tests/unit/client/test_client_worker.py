import threading
import time

import pytest

import beatbot.client.worker
import beatbot.constants as constant
import beatbot.exceptions as exception


def test_sender_threading_event_property(sender_threading):
    assert isinstance(sender_threading.event, threading._Event)


def test_sender_threading_run(mocked_logger, sender_threading):
    try:
        sender_threading.start()
        time.sleep(1)
        sender_threading._socket.sendto.assert_called_with(
            constant.UDP_DATA,
            (sender_threading._server_host, sender_threading._server_port),
        )
        sender_threading.event.clear()
    finally:
        sender_threading._Thread__stop()


def test_get_sender(host_server):
    sender = beatbot.client.worker.get_sender(host_server, constant.LISTENER_PORT)
    assert isinstance(sender,beatbot.client.worker.SenderThreading)
