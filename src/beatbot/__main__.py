import argparse
import logging
import os
import signal
import sys
import threading
import time

import tiffany
import tiffany.utils as tiffany_util

import beatbot.constants as constant
import beatbot.exceptions as exception


logger = tiffany.get_logger(__name__)


def parse_argument():
    parser = argparse.ArgumentParser(
        prog="beatbot",
        epilog=("Example:\n" "   beatbot server\n" "   beatbot client\n"),
        formatter_class=argparse.RawTextHelpFormatter,
    )
    subparser = parser.add_subparsers()
    parser.add_argument(
        "--host", type=str, default=constant.LISTENER_HOST, help="Host address."
    )
    parser.add_argument(
        "--port", type=int, default=constant.LISTENER_PORT, help="Port number."
    )
    server_subparser = subparser.add_parser("server", help="Server sub-command.")
    server_subparser.set_defaults(func=_start_server)

    client_subparser = subparser.add_parser("client", help="Client sub-command.")
    client_subparser.set_defaults(func=_start_client)
    return parser.parse_args()


def _start_flask_app(app, host, port):
    import waitress

    if eval(os.getenv("BEATBOT_DEVELOPMENT", False)):
        # Set use_reloader to False to avoid "signal only works in the main thread" issue.
        # https://stackoverflow.com/questions/53522052/flask-app-valueerror-signal-only-works-in-main-thread
        app.run(host=host, port=port, use_reloader=False, threaded=True)
    else:
        waitress.serve(app, host=host, port=port)


def _validate_timeout_and_interval():
    interval = int(os.getenv("BEATBOT_SENDER_INTERVAL", 30))
    timeout = int(os.getenv("BEATBOT_UPDATER_TIMEOUT", 15))
    if not interval < timeout:
        msg = "Timeout ({}) is set smaller than the interval ({})!".format(
            timeout, interval
        )
        logger.error(msg, timeout=timeout, interval=interval)
        raise exception.ConfigError(msg)


def _start_server(args):
    import flask
    import flask_restful

    import beatbot.server.api as server_api
    import beatbot.server.cache as server_cache
    import beatbot.server.utils as server_util
    import beatbot.server.worker as server_worker

    _validate_timeout_and_interval()

    app = flask.Flask(__name__)

    listener_threading = server_worker.get_listener(
        args.host,
        args.port,
        server_util.ResourceManager(
            server_cache.BeatbotCache(
                os.getenv("BEATBOT_REDIS_HOST", "beatbot_redis_master"),
                int(os.getenv("BEATBOT_REDIS_PORT", 6379)),
                os.getenv("BEATBOT_REDIS_PASSWORD", "beatbot-pass"),
            ),
        ),
    )
    signal.signal(signal.SIGINT, listener_threading._stop)

    api = flask_restful.Api(app)
    server_api.Hosts.manager = listener_threading.manager
    server_api.Host.manager = listener_threading.manager
    api.add_resource(server_api.Hosts, constant.HOSTS_PATH)
    api.add_resource(
        server_api.Host, "{path}/<string:primary_key>".format(path=constant.HOSTS_PATH)
    )

    flask_thread = threading.Thread(
        name="beatbot_flask_app_threading",
        target=_start_flask_app,
        args=(app, args.host, int(os.getenv("BEATBOT_FLASK_PORT", 5000)),),
    )
    flask_thread.setDaemon(True)

    flask_thread.start()
    time.sleep(1)
    listener_threading.start()
    time.sleep(1)
    listener_threading.checker_thread.start()

    try:
        while listener_threading.is_alive():
            # This is to suppress the cpu utilization
            time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(0)


def _start_client(args):
    import beatbot.client.worker as client_worker

    _validate_timeout_and_interval()
    sender_threading = client_worker.get_sender(args.host, args.port,)
    signal.signal(signal.SIGINT, sender_threading._stop)
    sender_threading.start()

    try:
        while sender_threading.is_alive():
            # This is to suppress the cpu utilization
            time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        sys.exit(1)


def main():
    args = parse_argument()
    if tiffany_util.is_debug():
        logger.set_stream_handler_level(logging.DEBUG)
        logger.debug("Listener - DEBUG MODE")
    else:
        logger.set_stream_handler_level(logging.INFO)
    args.func(args)
    return 0


if __name__ == "__main__":
    sys.exit(main())
