import os
import socket
import threading
import time
import sys
import signal
from datetime import datetime

import tiffany
import tiffany.utils as tiffany_util

import beatbot.constants as constant


logger = tiffany.get_logger(__name__)


__all__ = ["SenderThreading", "get_sender"]


class SenderThreading(threading.Thread):
    def __init__(self, host, port):
        """
        Args:
            host (str):
            port (int):
        """
        super(SenderThreading, self).__init__()
        self._event = threading.Event()
        self._server_host = host
        self._server_port = port
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._host = socket.gethostbyname(socket.gethostname())
        self._max_retry = 5

        self.daemon = True
        self.interval = int(os.getenv("BEATBOT_SENDER_INTERVAL", 15))

    @property
    def event(self):
        return self._event

    def _stop(self):
        logger.info("Stop beatbot sender {}".format(self._host))
        self._event.clear()

    def run(self):
        logger.info(
            "PID {pid} - Start beatbot sender {host}", pid=os.getpid(), host=self._host
        )
        logger.info("Sender interval is set to {} second(s)".format(self.interval))
        while self._event.isSet():
            self._socket.sendto(
                constant.UDP_DATA, (self._server_host, self._server_port)
            )
            if tiffany_util.is_debug():
                logger.debug(
                    "{data} was sent to {host}:{port} at {time}",
                    data=constant.UDP_DATA,
                    host=self._server_host,
                    port=self._server_port,
                    time=datetime.fromtimestamp(
                        time.time()
                    ).strftime(constant.TIME_STRING_FORMAT),
                )
            time.sleep(self.interval)


def get_sender(host, port):
    """Factory function to return SenderThreading instance.

    Args:
        host (str):
        port (int):

    Returns:
        SenderThreading
    """
    sender_thread = SenderThreading(host, port)
    sender_thread.event.set()
    return sender_thread
