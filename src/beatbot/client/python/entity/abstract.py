import httplib

import beatbot.constants as constant


__all__ = ["AbstractEntity"]


class AbstractEntity(object):
    def __init__(self, host, port=None):
        self._domain = "{scheme}{host}:{port}".format(
            scheme=constant.URL_SCHEME, host=host, 
            port=port or constant.DEFAULT_FLASK_PORT
        )

    @staticmethod
    def _respond_handler(respond, fallback=None):
        if respond.status_code == httplib.OK:
            return respond.json()
        elif respond.status_code in {httplib.NOT_FOUND, httplib.INTERNAL_SERVER_ERROR}:
            return fallback if not fallback is None else []
        else:
            raise NotImplementedError(
                "Respond status handler has not been implemented: {}".format(
                    respond.status_code
                )
            )

    @property
    def domain(self):
        return self._domain

    @property
    def hosts_path(self):
        return "{}{}".format(self._domain, constant.HOSTS_PATH)
