import os

import requests
import tiffany

from beatbot.client.entity import abstract


__all__ = ["OutputEntity"]

logger = tiffany.get_logger(__name__)


class OutputEntity(abstract.AbstractEntity):
    def get_heartbeats(self):
        """Return all registered host and its state.

        Returns:
            dict[str: dict[str: float, str: bool]]
        """
        try:
            respond = requests.get(self.hosts_path)
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})

    def get_heartbeat(self, primary_key):
        """Return requested host and its state.

        Args:
            primary_key (str):

        Returns:
            dict[str: float, str: bool]
        """
        try:
            respond = requests.get(os.path.join(self.hosts_path, primary_key))
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})

    def insert_heartbeat(self, fields):
        """
        Args:
            primary_key (str):
            fields (dict[str: str, str: float, str: bool]): For example:
                {"host": "192.168.0.21", "last_connected": 1607032848.721141, "state": True}

        Returns:
            dict[str: str, str: float, str: bool]
        """
        try:
            respond = requests.post(self.hosts_path, fields)
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})

    def update_heartbeat(self, primary_key, fields):
        """
        Args:
            primary_key (str):
            fields (dict[str: str, str: float, str: bool]): For example:
                {"host": "192.168.0.21", "last_connected": 1607032848.721141, "state": True}

        Returns:
            dict[str: str, str: float, str: bool]: For example:
                {"url": "/path/to/url", "last_connected": 1607032848.721141, "state": True}
        """
        try:
            respond = requests.put(os.path.join(self.hosts_path, primary_key), fields)
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})

    def delete_heartbeat(self, primary_key):
        """
        Args:
            primary_key (str):

        Returns:
            int: HTTP return status code
        """
        try:
            respond = requests.delete(os.path.join(self.hosts_path, primary_key))
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})

    def delete_heartbeats(self):
        """
        Returns:
            int: HTTP return status code
        """
        try:
            respond = requests.delete(self.hosts_path)
        except requests.exceptions.ConnectionError:
            logger.critical(
                "Unknown service, connection error to: {path}", path=self.hosts_path
            )
            raise
        else:
            return self._respond_handler(respond, fallback={})
