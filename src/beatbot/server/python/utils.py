import threading
import time

import beatbot.exceptions as exception


__all__ = ["ResourceManager"]


class ResourceManager(object):
    def __init__(self, cache):
        """
        Args:
            cache (BeatbotCache):
        """
        self._cache = cache
        self._lock = threading.Lock()

    def check_resources(self, interval):
        """Check if the last connection time is smaller than the timeout,
        then mark the as disconnected.

        Args:
            interval (int): Checker internval in seconds.
        """
        resources = self.get_resources()
        if resources:
            timeout = time.time() - interval
            for primary_key, state_data in resources.items():
                if state_data["last_connected"] < timeout:
                    self.set_resource(
                        primary_key,
                        {
                            "last_connected": state_data["last_connected"],
                            "state": False,
                        },
                    )
        return resources

    def get_resource(self, primary_key):
        """
        Args:
            primary_key (str):
        """
        with self._lock:
            return self._cache.get_one(primary_key)

    def get_resources(self):
        with self._lock:
            return self._cache.get()

    def set_resource(self, primary_key, data):
        """
        Args:
            primary_key (str):
            data (dict)
        """
        with self._lock:
            self._cache.set(primary_key, data)

    def delete_resources(self):
        with self._lock:
            self._cache.delete()

    def delete_resource(self, primary_key):
        """
        Args:
            primary_key (str):
        """
        with self._lock:
            self._cache.delete_one(primary_key)
            