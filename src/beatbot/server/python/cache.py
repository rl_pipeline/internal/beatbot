import cPickle
from abc import ABCMeta, abstractmethod

import redis

import beatbot.exceptions as exception


__all__ = ["BeatbotCache"]


class AbstractData(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def get(self):
        pass

    @abstractmethod
    def get_one(self, key):
        pass

    @abstractmethod
    def set(self, key, values):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def delete_one(self, key):
        pass


class BeatbotCache(AbstractData):
    def __init__(self, host, port, password):
        """
        Args:
            host (str):
            port (int):
            password (str):
        """
        self._cache = redis.StrictRedis(host=host, port=port, password=password)

    def get_one(self, key):
        """
        Args:
            key (str):

        Returns:
            dict[str: float, str: bool]
        """
        try:
            value = self._cache.get(key)
        except redis.exceptions.ResponseError as e:
            raise exception.ServerCacheResponseError(e)
        except redis.exceptions.ConnectionError as e:
            raise exception.ServerCacheConnectionError(e)
        else:
            try:
                return cPickle.loads(value)
            except TypeError:
                pass

    def set(self, key, values):
        """
        Args:
            key (str):
            values (dict):
        """
        try:
            self._cache.set(key, cPickle.dumps(values))
        except redis.exceptions.ResponseError as e:
            raise exception.ServerCacheResponseError(e)
        except redis.exceptions.ConnectionError as e:
            raise exception.ServerCacheConnectionError(e)

    def get(self):
        """
        Returns:
            dict[str: dict[str: float, str: bool]]
        """
        try:
            return {key: self.get_one(key) for key in self._cache.keys("*")}
        except redis.exceptions.ResponseError as e:
            raise exception.ServerCacheResponseError(e)
        except redis.exceptions.ConnectionError as e:
            raise exception.ServerCacheConnectionError(e)

    def delete(self):
        try:
            for key in self._cache.keys("*"):
                self._cache.delete(key)
        except redis.exceptions.ResponseError as e:
            raise exception.ServerCacheResponseError(e)
        except redis.exceptions.ConnectionError as e:
            raise exception.ServerCacheConnectionError(e)

    def delete_one(self, key):
        """
        Args:
            key (str):
        """
        try:
            self._cache.delete(key)
        except redis.exceptions.ResponseError as e:
            raise exception.ServerCacheResponseError(e)
        except redis.exceptions.ConnectionError as e:
            raise exception.ServerCacheConnectionError(e)
