import os
import socket
import threading
import time
from datetime import datetime

import tiffany
import tiffany.utils as tiffany_util

import beatbot.constants as constant
import beatbot.exceptions as exception


logger = tiffany.get_logger(__name__)


__all__ = ["ListenerThreading", "CheckerThreading", "get_listener"]


class ListenerThreading(threading.Thread):
    def __init__(self, manager, host, port):
        """
        Args:
            manager (ResourceManager):
            host (str):
            port (int):
            timeout (int): Connection timeout in second.
        """
        super(ListenerThreading, self).__init__()
        self._manager = manager
        self._event = threading.Event()
        self._host = host
        self._port = port
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self._socket.bind((host, port))
            logger.info("Listener socket bind to: {host}:{port}", host=host, port=port)
        except socket.error:
            logger.exception(
                "ListenerSocketBindError {host}:{port}", host=host, port=port
            )
            raise exception.ListenerSocketBindError("{}:{}".format(host, port))
        # A Thread blocked indefenitely on an I/O operation may never return to
        # check if its been killed, so we set timeout for the loop.
        self._socket.settimeout(5)

        self._checker_thread = CheckerThreading(
            manager, 
            interval=int(
                os.getenv("BEATBOT_CHECKER_INTERVAL", constant.CHECKER_INTERVAL)
            ),
        )
        self._checker_thread.event.set()

        self.name = "beatbot_listener_threading"
        # Daemon thread doesn't block the main thread from exiting and continues
        # to run in the background.
        self.setDaemon(True)

    @property
    def event(self):
        return self._event

    @property
    def manager(self):
        return self._manager

    @property
    def checker_thread(self):
        return self._checker_thread

    def _stop(self, signal, frame):
        self._checker_thread._stop()
        logger.info("Stop beatbot server {}:{}".format(self._host, self._port))
        self._event.clear()
        # Wait until the thread terminates.
        self.join()

    def run(self):
        logger.info(
            "PID {} - Start beatbot server {}:{}".format(
                os.getpid(), self._host, self._port
            )
        )
        while self._event.isSet():
            try:
                data, socket_ = self._socket.recvfrom(len(constant.UDP_DATA))
            except socket.timeout:
                # If socket hasn't received any data before the timeout, catch
                # the timeout exception and continue.
                continue
            else:
                host, _ = socket_
                # This is to handle localhost connection
                host = "127.0.1.1" if host == "127.0.0.1" else host
                try:
                    self._manager.set_resource(
                        host, {"last_connected": time.time(), "state": True}
                    )
                except (
                    exception.ServerCacheResponseError,
                    exception.ServerCacheConnectionError,
                ) as e:
                    logger.critical(
                        "Skip set resource, cache server error: {error}", error=e
                    )
                    time.sleep(1)
                else:
                    if tiffany_util.is_debug():
                        logger.debug(
                            "Beatbot listener received '{data}' from '{host}'",
                            data=data,
                            host=host,
                        )


class CheckerThreading(threading.Thread):
    def __init__(self, manager, interval=constant.CHECKER_INTERVAL):
        """
        Args:
            manager (ResourceManager):
            interval (int): Checker interval in second.
        """
        super(CheckerThreading, self).__init__()
        self._manager = manager
        self._event = threading.Event()
        self.interval = interval
        self.name = "beatbot_checker_threading"
        self.setDaemon(True)

    @property
    def event(self):
        return self._event

    def _stop(self):
        logger.info("Stop beatbot checker..")
        self._event.clear()
        # Wait until the thread terminates.
        self.join()

    def run(self):
        logger.info("Start beatbot checker..")
        logger.info("Checker interval is set to {} second(s)".format(self.interval))
        while self._event.isSet():
            if tiffany_util.is_debug():
                logger.debug(
                    "Run beatbot checker at {}".format(
                        datetime.fromtimestamp(time.time()).strftime(constant.TIME_STRING_FORMAT)
                    )
                )
            try:
                self._manager.check_resources(self.interval)
            except (
                exception.ServerCacheResponseError,
                exception.ServerCacheConnectionError,
            ) as e:
                logger.critical(
                    "Skip checking state, cache server error: {error}", error=e
                )
            else:
                time.sleep(self.interval)


def get_listener(host, port, manager):
    """Factory function to return ListenerThreading instance.
    
    Args:
        host (str):
        port (int):
        manager (ResourceManager):

    Returns:
        ListenerThreading
    """
    hb_thread = ListenerThreading(manager, host, port)
    hb_thread.event.set()
    return hb_thread
