import httplib

import flask
import flask_restful
import tiffany
import tiffany.utils as tiffany_util
from flask import jsonify, make_response, request
from flask_restful import reqparse

import beatbot.constants as constant
import beatbot.exceptions as exception


logger = tiffany.get_logger(__name__)


__all__ = ["Hosts", "Host", "parse_post_args", "parse_put_args"]


def parse_post_args():
    parser = reqparse.RequestParser()
    parser.add_argument(
        constant.PRIMARY_KEY, required=True, type=str, help=constant.PRIMARY_KEY
    )
    parser.add_argument("state", required=True, type=bool, help="state")
    parser.add_argument(
        "last_connected", required=True, type=float, help="last_connected"
    )
    return parser.parse_args()


def parse_put_args():
    parser = reqparse.RequestParser()
    parser.add_argument("last_connected", type=float, help="last_connected")
    parser.add_argument("state", type=bool, help="state")
    return parser.parse_args()


class Hosts(flask_restful.Resource):

    manager = None

    def get(self):
        try:
            data = self.__class__.manager.get_resources()
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("GET - Database Connection Error!")
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            if data:
                if tiffany_util.is_debug():
                    logger.debug("GET - Resources: {resource}.", resource=data)
                return data, httplib.OK
            else:
                msg = "No {} heartbeat found on database!".format(constant.PRIMARY_KEY)
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {}".format(msg))
                flask.abort(httplib.NOT_FOUND, msg)

    def post(self):
        args = parse_post_args()
        try:
            self.__class__.manager.set_resource(
                args.host, {"state": args.state, "last_connected": args.last_connected}
            )
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("POST - Database Connection Error!")
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            if tiffany_util.is_debug():
                logger.debug("POST - Resource: {resource}", resource=args)
            return args, httplib.OK

    def delete(self):
        try:
            self.__class__.manager.delete_resources()
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("DELETE - Database Connection Error!")
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            if tiffany_util.is_debug():
                logger.debug("DELETE - Workstations have been deleted.")
            return httplib.OK


class Host(flask_restful.Resource):

    manager = None

    def get(self, primary_key):
        """
        Args:
            primary_key (str):

        Returns:
            dict[str: dict[str: float, str: bool]]
        """
        try:
            data = self.__class__.manager.get_resource(primary_key)
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("GET - Database Connection Error!")
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            if data:
                if tiffany_util.is_debug():
                    logger.debug("GET - Resources: {resource}", resource=data)
                return data, httplib.OK
            else:
                msg = "Heartbeat - Hostname '{}' not found on database!".format(
                    primary_key
                )
                if tiffany_util.is_debug():
                    logger.debug("GET - Resource: {}".format(msg))
                flask.abort(httplib.NOT_FOUND, msg)

    def put(self, primary_key):
        args = parse_put_args()
        try:
            self.__class__.manager.set_resource(primary_key, args)
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("PUT - Database Operational Error: {error}", error=e)
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            args["url"] = request.base_url
            if tiffany_util.is_debug():
                logger.debug("PUT - Resource: {resource}", resource=args)
            return make_response(jsonify({"url": request.base_url}), httplib.OK)

    def delete(self, primary_key):
        try:
            self.__class__.manager.delete_resource(primary_key)
        except (
            exception.ServerCacheResponseError,
            exception.ServerCacheConnectionError,
        ) as e:
            logger.exception("DELETE - Database Operational Error: {error}", error=e)
            flask.abort(httplib.INTERNAL_SERVER_ERROR)
        else:
            msg = "DELETE - Resource: {}".format(request.base_url)
            if tiffany_util.is_debug():
                logger.debug(msg)
            return make_response(jsonify({"url": request.base_url}), httplib.OK)
