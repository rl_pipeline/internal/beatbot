Flask==1.1.2
Flask-RESTful==0.3.8
mock==3.0.5
pytest==4.6
pytest-cov==2.10.1
redis==3.5.3
waitress==1.4.4