# Collect python files
file(
    GLOB_RECURSE 
    python_files 
    "${CMAKE_CURRENT_SOURCE_DIR}/python/*.py"
)

stage_files(
    FILEPATHS
        ${python_files}
    DESTINATION
        "\
${PROJECT_NAME}/${RL_INSTALL_LIBDIR}/python\
$ENV{PYTHON2_VERSION_MAJOR}.$ENV{PYTHON2_VERSION_MINOR}/site-packages/${PROJECT_NAME}/server"
    # CREATE_SYMLINKS
)

install(
    DIRECTORY
        ${RL_STAGING_DIR}/${PROJECT_NAME}
    DESTINATION
        ${CMAKE_INSTALL_PREFIX}
    USE_SOURCE_PERMISSIONS
    PATTERN "tests" EXCLUDE
    PATTERN "*.pyc" EXCLUDE
)