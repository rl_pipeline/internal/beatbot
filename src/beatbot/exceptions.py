class ServerCacheResponseError(Exception):
    pass


class ServerCacheConnectionError(Exception):
    pass


class ServerModelOperationError(Exception):
    pass


class ListenerSocketBindError(Exception):
    pass
